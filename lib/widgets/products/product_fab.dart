import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../models/product.dart';
import '../../scoped-models/main.dart';

class ProductFAB extends StatefulWidget {
  final Product product;

  ProductFAB({this.product});

  @override
  State<StatefulWidget> createState() {
    return _ProductFAB();
  }
}

class _ProductFAB extends State<ProductFAB> with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ScaleTransition(
              scale: CurvedAnimation(
                parent: _controller,
                curve: Interval(0.0, 1.0, curve: Curves.easeOut),
              ),
              child: Container(
                height: 70.0,
                width: 56.0,
                alignment: FractionalOffset.topCenter,
                child: FloatingActionButton(
                  backgroundColor: Theme.of(context).cardColor,
                  mini: true,
                  heroTag: 'contact',
                  onPressed: () async {
                    final url = 'mailto:${widget.product.userEmail}';

                    if (await canLaunch(url)) {
                      await launch(url);
                    } else {
                      throw 'Could not launch!';
                    }
                  },
                  child: Icon(
                    Icons.mail_outline,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ),
            ScaleTransition(
              scale: CurvedAnimation(
                parent: _controller,
                curve: Interval(0.0, 0.5, curve: Curves.easeOut),
              ),
              child: Container(
                height: 70.0,
                width: 56.0,
                alignment: FractionalOffset.topCenter,
                child: FloatingActionButton(
                  backgroundColor: Theme.of(context).cardColor,
                  mini: true,
                  heroTag: 'favourite',
                  onPressed: () {
                    model.toggleProductFavourite();
                  },
                  child: Icon(
                    model.selectedProduct.isFavourite
                        ? Icons.favorite
                        : Icons.favorite_border,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ),
            Container(
              height: 70.0,
              width: 56.0,
              child: FloatingActionButton(
                heroTag: 'options',
                onPressed: () {
                  if (_controller.isDismissed) {
                    _controller.forward();
                  } else {
                    _controller.reverse();
                  }
                },
                child: AnimatedBuilder(
                    animation: _controller,
                    builder: (BuildContext context, Widget child) {
                      return Transform(
                        child: Icon(_controller.isDismissed
                            ? Icons.more_vert
                            : Icons.close),
                        alignment: FractionalOffset.center,
                        transform: Matrix4.rotationZ(
                            _controller.value * 0.5 * math.pi),
                      );
                    }),
              ),
            ),
          ],
        );
      },
    );
  }
}
