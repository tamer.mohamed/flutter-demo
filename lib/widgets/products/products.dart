import 'package:flutter/material.dart';

import '../products/products_card.dart';
import '../../models/product.dart';

class Products extends StatelessWidget {
  final List<Product> products;
  final Function fetchProducts;

  Products({this.products, this.fetchProducts});

  Widget _buildProductList(context, products) {
    Widget productCard = Container();

    if (products.length > 0) {
      productCard = ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ProductsCard(
            productIndex: index,
            product: products[index],
          );
        },
        itemCount: products.length,
      );
    } else {
      productCard = Center(
        child: Text('No product listed'),
      );
    }

    return RefreshIndicator(
      child: productCard,
      onRefresh: fetchProducts,
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildProductList(context, products);
  }
}
