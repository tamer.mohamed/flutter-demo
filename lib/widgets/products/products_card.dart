import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:map_view/map_view.dart';

import '../../scoped-models/main.dart';
import './title_default.dart';
import '../../models/product.dart';

class ProductsCard extends StatelessWidget {
  final Product product;
  final int productIndex;

  ProductsCard({this.product, this.productIndex});

  @override
  Widget build(BuildContext context) => ScopedModelDescendant(
        builder: (BuildContext context, Widget widget, MainModel model) =>
            GestureDetector(
              onTap: () {
                model.setSelectedProduct(product.id);

                Navigator.pushNamed<bool>(
                  context,
                  '/product/${product.id}',
                ).then((_) {
                  model.setSelectedProduct(null);
                });
              },
              child: Card(
                borderOnForeground: true,
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                child: Stack(
                  children: <Widget>[
                    Hero(
                      tag: product.id,
                      child: FadeInImage(
                          placeholder:
                              AssetImage('assets/product-placeholder.jpeg'),
                          image: NetworkImage(product.imageUrl),
                          fit: BoxFit.cover,
                          height: 300,
                          width: getCardWidth(context)),
                    ),
                    Positioned(
                      top: 200,
                      child: Container(
                        width: getCardWidth(context),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [Colors.transparent, Colors.black],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(height: 10.0),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: getCardWidth(context) * 0.6,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      TitleDefault(
                                          title: product.title,
                                          color: Colors.white),
                                      SizedBox(
                                        height: 10.0,
                                      ),
                                      GestureDetector(
                                        child: Text(
                                          product.location.address,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                        onTap: _showMap,
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 10.0),
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5.0),
                                      bottomLeft: Radius.circular(5.0),
                                    ),
                                  ),
                                  child: Text(
                                    '\$${product.price}',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 20.0),
//                            ButtonBar(
//                              alignment: MainAxisAlignment.center,
//                              children: <Widget>[
//                                IconButton(
//                                  icon: Icon(product.isFavourite
//                                      ? Icons.favorite
//                                      : Icons.favorite_border),
//                                  color: Colors.red,
//                                  iconSize: 20,
//                                  onPressed: () {
//                                    model.setSelectedProduct(product.id);
//                                    model.toggleProductFavourite();
//                                    model.setSelectedProduct(null);
//                                  },
//                                )
//                              ],
//                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
      );

  void _showMap() {
    final markers = <Marker>[
      Marker(
        'loc',
        'location',
        product.location.lat,
        product.location.lng,
      )
    ];
    final mapView = MapView();
    final cameraPosition = CameraPosition(
        Location(product.location.lat, product.location.lng), 14.0);

    mapView.show(
        MapOptions(
          mapViewType: MapViewType.normal,
          initialCameraPosition: cameraPosition,
          title: 'Product location',
        ),
        toolbarActions: [ToolbarAction('close', 1)]);

    mapView.onToolbarAction.listen((int id) {
      if (id == 1) {
        mapView.dismiss();
      }
    });

    mapView.onMapReady.listen((_) {
      mapView.setMarkers(markers);
    });
  }

  double getCardWidth(BuildContext context) {
    final double cardWidth = MediaQuery.of(context).size.width;

    return cardWidth;
  }
}
