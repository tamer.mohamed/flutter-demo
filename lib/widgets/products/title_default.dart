import 'package:flutter/material.dart';

class TitleDefault extends StatelessWidget {
  final String title;
  final Color color;

  TitleDefault({this.title, this.color});

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Text(
      title,
      style: TextStyle(
          fontSize: deviceWidth > 700 ? 35.0 : 26.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'Oswald',
          color: color != null ? color : Colors.black),
        textAlign: TextAlign.left,

    );
  }
}
