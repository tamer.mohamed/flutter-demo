import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';
import 'dart:convert';
import 'dart:async';
import 'package:location/location.dart' as geoloc;
import 'package:http/http.dart' as http;

import '../../models/location_data.dart';
import '../../shared/global_config.dart';

import '../helpers/ensure-visible.dart';

class LocationField extends StatefulWidget {
  final Function setLocation;
  final LocationData initialValue;

  LocationField({this.setLocation, this.initialValue});

  @override
  State<StatefulWidget> createState() {
    return _LocationState();
  }
}

class _LocationState extends State<LocationField> {
  final FocusNode _locationInputFocusNode = FocusNode();
  LocationData _locationData;
  final TextEditingController _locationInputController =
      TextEditingController();
  Uri _staticMapUri;

  @override
  void initState() {
    _locationInputFocusNode.addListener(_updateLocation);

    if (widget.initialValue != null) {
      _getStaticMap(widget.initialValue.address, geocode: false);
    }

    super.initState();
  }

  Future<String> _getAddress(double lng, double lat) async {
    final Uri uri = Uri.https('maps.googleapis.com', '/maps/api/geocode/json', {
      'latlng': '$lat,$lng',
      'key': API_KEY
    });

    try {
      final http.Response response = await http.get(uri);
      final responseBody = json.decode(response.body);

      final formattedAddress = responseBody['results'][0]['formatted_address'];

      return formattedAddress;
    } catch (e) {
      //TODO: show dialog
      return null;
    }
  }

  void _getUserLocation() async {
    final location = geoloc.Location();
    try {
      final currentLocation = await location.getLocation();

      final address = await _getAddress(
          currentLocation.longitude, currentLocation.latitude);

      _getStaticMap(
        address,
        lat: currentLocation.latitude,
        lng: currentLocation.longitude,
      );
    } catch (error) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Couldn\'t fetch user location'),
              content: Text('Please add an address manually'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            );
          });
    }
  }

  void _getStaticMap(String address,
      {double lng, double lat, bool geocode = true}) async {
    if (address.isEmpty) {
      setState(() {
        _staticMapUri = null;
      });

      widget.setLocation(null);

      return;
    }

    if (geocode) {
      final Uri uri = Uri.https(
          'maps.googleapis.com', '/maps/api/geocode/json', {
        'address': address,
        'key': API_KEY
      });

      try {
        final http.Response response = await http.get(uri);

        if (response.statusCode != 200) {
          showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: Text('Something went wrong'),
                  content: Text("Couldn't fetch address information"),
                ),
          );
        }

        final Map<String, dynamic> responseBody = json.decode(response.body);

        final String formattedAddress =
            responseBody['results'][0]['formatted_address'];
        final coords = responseBody['results'][0]['geometry']['location'];

        _locationData = LocationData(
          lat: coords['lat'],
          lng: coords['lng'],
          address: formattedAddress,
        );
      } catch (error) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Couldn\'t fetch user location'),
                content: Text('Please add an address manually'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
      }
    } else if (lat == null && lng == null) {
      _locationData = widget.initialValue;
    } else {
      _locationData = LocationData(lng: lng, lat: lat, address: address);
    }

    if (mounted) {
      final StaticMapProvider staticMapProvider =
          StaticMapProvider(API_KEY);
      final Uri staticMapUri = staticMapProvider.getStaticUriWithMarkers(
        [
          Marker(
            'loc',
            'Location',
            _locationData.lat,
            _locationData.lng,
          ),
        ],
        width: 500,
        height: 300,
        center: Location(_locationData.lat, _locationData.lng),
        maptype: StaticMapViewType.roadmap,
      );

      widget.setLocation(_locationData);

      setState(() {
        _locationInputController.text = _locationData.address;
        _staticMapUri = staticMapUri;
      });
    }
  }

  @override
  void dispose() {
    _locationInputFocusNode.removeListener(_updateLocation);
    super.dispose();
  }

  void _updateLocation() {
    if (!_locationInputFocusNode.hasFocus) {
      _getStaticMap(_locationInputController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        EnsureVisibleWhenFocused(
          child: TextFormField(
            focusNode: _locationInputFocusNode,
            controller: _locationInputController,
            decoration: InputDecoration(labelText: 'Address'),
            validator: (String value) {
              if (_locationData == null || value.isEmpty) {
                return 'No valid location found';
              }
            },
          ),
          focusNode: _locationInputFocusNode,
        ),
        SizedBox(
          height: 10.0,
        ),
        FlatButton(
          child: Text('Use my location'),
          onPressed: _getUserLocation,
        ),
        SizedBox(
          height: 10.0,
        ),
        _staticMapUri == null ? Container() : Image.network('$_staticMapUri')
      ],
    );
  }
}
