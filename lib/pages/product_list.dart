import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../pages/product_edit.dart';
import '../scoped-models/main.dart';
import '../models/product.dart';

class ProductList extends StatefulWidget {
  final MainModel model;

  ProductList({this.model});

  @override
  State<StatefulWidget> createState() {
    return _MyProductListState();
  }
}

class _MyProductListState extends State<ProductList> {
  @override
  void initState() {
    widget.model.fetchProducts(clearExisting: true);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget widget, MainModel model) {
        final List<Product> products = model.allProducts;

        return ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            Product product = products[index];

            return Dismissible(
              key: Key(product.title),
              background: Container(
                color: Colors.red,
              ),
              onDismissed: (DismissDirection direction) {
                if (direction == DismissDirection.endToStart) {
                  model.setSelectedProduct(product.id);
                  model.deleteProduct();
                } else if (direction == DismissDirection.startToEnd) {
                  print('Swiped start to end');
                }
              },
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                        product.imageUrl,
                      ),
                    ),
                    title: Text(product.title),
                    subtitle: Text('\$${product.price}'),
                    trailing: _buildEditButton(context, index),
                  ),
                  Divider()
                ],
              ),
            );
          },
          itemCount: products.length,
        );
      },
    );
  }

  Widget _buildEditButton(context, index) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget widget, MainModel model) {
        return IconButton(
          icon: Icon(Icons.edit),
          onPressed: () {
            model.setSelectedProduct(model.allProducts[index].id);
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) {
                  return ProductEditPage();
                },
              ),
            ).then((_) {
              model.setSelectedProduct(null);
            });
          },
        );
      },
    );
  }
}
