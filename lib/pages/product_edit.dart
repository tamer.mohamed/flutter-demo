import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:io';

import '../widgets/helpers/ensure-visible.dart';
import '../widgets/form_elements/location.dart';
import '../widgets/form_elements/image.dart';
import '../scoped-models/main.dart';
import '../models/product.dart';
import '../models/location_data.dart';

class ProductEditPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProductEditPageState();
  }
}

class _ProductEditPageState extends State<ProductEditPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _textFocusNode = FocusNode();
  final FocusNode _descFocusNode = FocusNode();
  final FocusNode _priceFocusNode = FocusNode();
  final Map<String, dynamic> _formData = {
    'title': '',
    'desc': '',
    'price': null,
    'image': null,
    'location': null,
  };

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget widget, MainModel model) {
        Product product = model.selectedProduct;

        return _isEditMode(model)
            ? Scaffold(
                appBar: AppBar(
                  title: Text('Edit product ${product.title}'),
                ),
                body: _buildPageContent(context, model),
              )
            : _buildPageContent(context, model);
      },
    );
  }

  void _setImage(File image) {
    _formData['image'] = image;
  }

  Widget _buildPageContent(BuildContext context, MainModel model) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Container(
        margin: EdgeInsets.all(16.0),
        /* list views doesn't provide width,
       However we are using padding here as a workaround*/
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                _buildTitleTextFieldWidget(model),
                _buildDescTextFieldWidget(model),
                _buildPriceTextFieldWidget(model),
                SizedBox(
                  height: 10.0,
                ),
                ImageInput(
                    setImage: _setImage,
                    initialValue: _isEditMode(model)
                        ? model.selectedProduct.imageUrl
                        : null),
                SizedBox(
                  height: 10.0,
                ),
                LocationField(
                    setLocation: _setLocation,
                    initialValue: _isEditMode(model)
                        ? model.selectedProduct.location
                        : null),
                SizedBox(
                  height: 10.0,
                ),
                _buildSubmitButton(model),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTitleTextFieldWidget(MainModel model) {
    return EnsureVisibleWhenFocused(
      focusNode: _textFocusNode,
      child: TextFormField(
        focusNode: _textFocusNode,
        initialValue: _isEditMode(model) ? model.selectedProduct.title : '',
        decoration: InputDecoration(labelText: 'Title'),
        validator: (
          String value,
        ) {
          if (value.isEmpty || value.length < 5) {
            return 'Title is required and should be more than 5 charchters.';
          }
        },
        onSaved: (String value) {
          _formData['title'] = value;
        },
      ),
    );
  }

  Widget _buildDescTextFieldWidget(MainModel model) {
    return EnsureVisibleWhenFocused(
      focusNode: _descFocusNode,
      child: TextFormField(
        focusNode: _descFocusNode,
        initialValue: _isEditMode(model) ? model.selectedProduct.desc : '',
        decoration: InputDecoration(labelText: 'Description'),
        keyboardType: TextInputType.multiline,
        maxLines: null,
        validator: (
          String value,
        ) {
          if (value.isEmpty || value.length < 15) {
            return 'Description is required and should be more than 15 charchters.';
          }
        },
        onSaved: (String value) {
          _formData['desc'] = value;
        },
      ),
    );
  }

  Widget _buildPriceTextFieldWidget(MainModel model) {
    return EnsureVisibleWhenFocused(
      focusNode: _priceFocusNode,
      child: TextFormField(
        focusNode: _priceFocusNode,
        initialValue:
            _isEditMode(model) ? '${model.selectedProduct.price}' : '',
        decoration: InputDecoration(labelText: 'Price'),
        keyboardType: TextInputType.number,
        validator: (
          String value,
        ) {
          if (value.isEmpty ||
              !RegExp(r'^(?:[1-9]\d*|0)?(?:[.,]\d+)?$').hasMatch(value)) {
            return 'Price is required and should be a number.';
          }
        },
        onSaved: (String value) {
          _formData['price'] = value;
        },
      ),
    );
  }

  Widget _buildSubmitButton(MainModel model) {
    return model.isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : RaisedButton(
            child: Text('Save'),
            color: Theme.of(context).accentColor,
            textColor: Colors.white,
            onPressed: () => _handleSubmit(model),
          );
  }

  void _setLocation(LocationData location) {
    _formData['location'] = location;
  }

  void _handleSubmit(model) {
    if (!_formKey.currentState.validate() ||
        (_formData['image'] == null && !_isEditMode(model))) {
      return;
    }

    _formKey.currentState.save();

    if (_isEditMode(model)) {
      model
          .updateProduct(
            title: _formData['title'],
            desc: _formData['desc'],
            price: double.parse(
              _formData['price'].replaceFirst(RegExp(r','), '.'),
            ),
            image: _formData['image'],
            location: _formData['location'],
          )
          .then(
            (_) =>
                Navigator.pushReplacementNamed(context, '/products').then((_) {
                  model.setSelectedProduct(null);
                }),
          );
    } else {
      model
          .addProduct(
        title: _formData['title'],
        desc: _formData['desc'],
        price: double.parse(
          _formData['price'].replaceFirst(RegExp(','), '.'),
        ),
        image: _formData['image'],
        location: _formData['location'],
      )
          .then((bool success) {
        if (success) {
          Navigator.pushReplacementNamed(context, '/products').then((_) {
            model.setSelectedProduct(null);
          });
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Something went wrong'),
                content: Text('Please try again'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ],
              );
            },
          );
        }
      });
    }
  }

  bool _isEditMode(MainModel model) {
    return model.selectedProductIndex != -1;
  }
}
