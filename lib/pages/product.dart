import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:async';

import '../widgets/stars.dart';
import '../scoped-models/main.dart';
import '../models/product_popup_menu.dart';
import '../widgets/products/title_default.dart';
import '../widgets/products/product_fab.dart';
import '../models/product.dart';

class ProductPage extends StatelessWidget {
  final Product product;

  final List<ProductPopupMenu> choices = <ProductPopupMenu>[
    ProductPopupMenu(title: 'Add to favourite', icon: Icons.home),
    ProductPopupMenu(title: 'Report', icon: Icons.settings),
  ];

  ProductPage({this.product});

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget widget, MainModel model) {
      return WillPopScope(
          child: Scaffold(
            body: CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: 256.0,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: true,
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[Text(product.title)],
                    ),
                    background: Hero(
                      tag: product.id,
                      child: FadeInImage(
                        height: 300,
                        fit: BoxFit.cover,
                        placeholder:
                            AssetImage('assets/product-placeholder.jpeg'),
                        image: NetworkImage(product.imageUrl),
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate([
                    _buildTitleRatingRow(product),
                    SizedBox(
                      height: 5.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: _buildAddressPriceRow(product),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Text(product.desc),
                    ),
                  ]),
                )
              ],
            ),

//            bottomNavigationBar: _buildBottomNavigationBar(context),
            floatingActionButton: ProductFAB(product: product),
          ),
          onWillPop: () {
            Navigator.pop(context, false);

            // ignore the original action by passing `false`
            return Future.value(false);
          });
    });
  }

  Widget _buildAddressPriceRow(Product product) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconTheme(
          data: IconThemeData(
            color: Colors.orange,
            size: 20.0,
          ),
          child: StarDisplay(
            value: 4,
          ),
        ),
        SizedBox(width: 10.0),
        Text(
          '\$${product.price}',
          style: TextStyle(
              fontFamily: 'Oswald',
              fontWeight: FontWeight.w700,
              fontSize: 20.0,
              color: Colors.grey),
        ),
      ],
    );
  }

  Widget _buildTitleRatingRow(Product product) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            product.location.address,
            style: TextStyle(
                fontFamily: 'Oswald',
                fontWeight: FontWeight.w700,
                fontSize: 12.0,
                color: Colors.grey),
          ),
        ],
      ),
    );
  }
}
