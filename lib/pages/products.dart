import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../widgets/products/products.dart';
import '../widgets/logout_list_tile.dart';
import '../scoped-models/main.dart';

class ProductsPage extends StatefulWidget {
  final MainModel model;

  ProductsPage({this.model});

  @override
  State<StatefulWidget> createState() {
    return _MyProductsState();
  }
}

class _MyProductsState extends State<ProductsPage> {
  @override
  void initState() {
    super.initState();

    widget.model.fetchProducts();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget widget, MainModel model) {
      return Scaffold(
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
                title: Text('Main menu sss'),
              ),
              ListTile(
                leading: Icon(Icons.edit),
                title: Text('Manage products'),
                onTap: () {
                  Navigator.pushReplacementNamed(
                    context,
                    '/manage-product',
                  );
                },
              ),
              LogoutListTile(),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text(_getTabTitle(model)),
        ),
        body: _buildTabsBody(model),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          onTap: (int index) {
            model.setSelectedTab(index);
          },
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Colors.grey,
          currentIndex: model.currentSelectedTab,
          elevation: 20,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              title: Text('Favourites'),
            ),
            BottomNavigationBarItem(
              icon: Column(
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Icon(Icons.notifications_active),
                      Positioned(
                        right: -5,
                        top: -5,
                        child: Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints:
                              BoxConstraints(minWidth: 14, minHeight: 14),
                          child: Text(
                            '3',
                            style: TextStyle(color: Colors.white, fontSize: 8),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
              title: Text(
                'Cart',
              ),
            ),
          ],
        ),
      );
    });
  }

  String _getTabTitle(MainModel model) {
    switch (model.currentSelectedTab) {
      case 0:
        return 'Products';
      case 1:
        return 'Favourites';
      case 2:
        return 'My cart';
      default:
        return 'Products';
    }
  }

  Widget _buildTabsBody(MainModel model) {
    if (model.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    switch (model.currentSelectedTab) {
      case 0:
        return Products(
          products: model.allProducts,
          fetchProducts: model.fetchProducts,
        );
      case 1:
        return Products(
          products: model.getFavourites(),
          fetchProducts: model.fetchProducts,
        );
      default:
        return Container();
    }
  }
}
