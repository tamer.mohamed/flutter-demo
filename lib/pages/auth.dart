import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/animation.dart';

import '../models/auth.dart';
import '../scoped-models/main.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthPageState();
  }
}

class _AuthPageState extends State<AuthPage> with TickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordController = TextEditingController();
  final Map<String, dynamic> _formData = {
    'email': '',
    'password': '',
    'accepted': false
  };

  AnimationController _animationController;
  Animation<Offset> _slideAnimation;

  AuthMode _authMode = AuthMode.Login;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    _slideAnimation = Tween<Offset>(
      begin: Offset(0.0, -2.0),
      end: Offset.zero,
    ).animate(
        CurvedAnimation(parent: _animationController, curve: Curves.easeIn));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 500.0 ? 500.0 : deviceWidth * 0.95;

    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          color: Theme.of(context).primaryColor,
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                width: targetWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _buildEmailTextFieldWidget(),
                    SizedBox(
                      height: 10.0,
                    ),
                    _buildPasswordTextFieldWidget(),
                    SizedBox(
                      height: 10.0,
                    ),
                    _buildPasswordConfirmTextFieldWidget(),
                    _buildSwitchTileWidget(),
                    SizedBox(
                      height: 10.0,
                    ),
                    FlatButton(
                      child: Text(
                        'Switch to ${_authMode == AuthMode.Signup ? 'Login' : 'Signup'}',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        setState(() {
                          if (_authMode == AuthMode.Login) {
                            _authMode = AuthMode.Signup;

                            _animationController.forward();
                          } else {
                            _authMode = AuthMode.Login;
                            _animationController.reverse();
                          }
                        });
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        ScopedModelDescendant<MainModel>(
                          builder: (BuildContext context, Widget widget,
                              MainModel model) {
                            return model.isLoading
                                ? CircularProgressIndicator(
                                    backgroundColor: Colors.white,
                                  )
                                : RaisedButton(
                                    child: Text('Login'),
                                    onPressed: () => _handleSubmit(model),
                                  );
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  DecorationImage _buildBackgroundImage() {
    return DecorationImage(
      image: AssetImage('assets/background.jpg'),
      fit: BoxFit.cover,
      colorFilter:
          ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
    );
  }

  Widget _buildEmailTextFieldWidget() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Email',
        fillColor: Colors.white,
        filled: true,
      ),
      validator: (String email) {
        if (email.isEmpty ||
            !RegExp(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
                .hasMatch(email)) {
          return "Email is required.";
        }
      },
      onSaved: (String username) {
        _formData['email'] = username;
      },
      keyboardType: TextInputType.emailAddress,
    );
  }

  Widget _buildPasswordConfirmTextFieldWidget() {
    return FadeTransition(
      opacity: CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.0,
          1.0,
          curve: Curves.easeIn,
        ),
      ),
      child: SlideTransition(
        position: _slideAnimation,
        child: TextFormField(
          decoration: InputDecoration(
            labelText: 'Confirm password',
            fillColor: Colors.white,
            filled: true,
          ),
          obscureText: true,
          validator: (String email) {
            if (_passwordController.text != email &&
                _authMode == AuthMode.Signup) {
              return "Passwords don't match";
            }
          },
        ),
      ),
    );
  }

  Widget _buildPasswordTextFieldWidget() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Password',
        fillColor: Colors.white,
        filled: true,
      ),
      controller: _passwordController,
      validator: (String email) {
        if (email.isEmpty) {
          return "Password is required.";
        }
      },
      onSaved: (String password) {
        _formData['password'] = password;
      },
      obscureText: true,
    );
  }

  // FIXME: add validation when remember me is not checked
  Widget _buildSwitchTileWidget() {
    return SwitchListTile(
      title: Text('Remember me', style: TextStyle(color: Colors.white)),
      value: _formData['accepted'],
      onChanged: (bool value) {
        setState(() {
          _formData['accepted'] = value;
        });
      },
    );
  }

  void _handleSubmit(MainModel model) async {
    // switch and checkboxes are not connected with validate
    // at least for the current flutter version.
    if (!_formKey.currentState.validate() || !_formData['accepted']) {
      return;
    }

    _formKey.currentState.save();

    Map<String, dynamic> responseInformation;

    if (_authMode == AuthMode.Login) {
      responseInformation =
          await model.login(_formData['email'], _formData['password']);

      if (responseInformation['success']) {}
    } else {
      responseInformation =
          await model.signUp(_formData['email'], _formData['password']);
    }

    if (responseInformation['success']) {
      Navigator.pushReplacementNamed(
        context,
        '/',
      );
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error on signup'),
            content: Text(responseInformation['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Retry'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }
}
