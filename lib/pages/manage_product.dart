import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main.dart';
import 'product_edit.dart';
import 'product_list.dart';

class ManageProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget widget, MainModel model) {
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            drawer: Drawer(
              child: Column(
                children: <Widget>[
                  AppBar(
                    title: Text('Main menu'),
                  ),
                  ListTile(
                    leading: Icon(Icons.shop),
                    title: Text('Products'),
                    onTap: () {
                      Navigator.pushReplacementNamed(
                        context,
                        '/',
                      );
                    },
                  )
                ],
              ),
            ),
            appBar: AppBar(
              title: Text('Manage product'),
              bottom: TabBar(tabs: <Widget>[
                Tab(
                  child: Text('Create product'),
                  icon: Icon(Icons.create),
                ),
                Tab(
                  child: Text('My products'),
                  icon: Icon(Icons.list),
                )
              ]),
            ),
            body: TabBarView(
              children: [
                ProductEditPage(),
                ProductList(
                  model: model,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
