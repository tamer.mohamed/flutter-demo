import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:map_view/map_view.dart';
import 'package:flutter/services.dart';

import 'pages/manage_product.dart';
import 'pages/product.dart';
import 'pages/auth.dart';
import 'pages/products.dart';
import 'scoped-models/main.dart';
import 'models/product.dart';
import 'widgets/helpers/custom_route.dart';
import 'shared/global_config.dart';

void main() {
  MapView.setApiKey(API_KEY);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  final _platformChannel = MethodChannel('myapp.com/battery');
  bool _isAuthenticated = false;


  Future<Null> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await _platformChannel.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level is $result %.';
    } catch (error) {
      batteryLevel = 'Failed to get battery level.';
      print(error);
    }
    print(batteryLevel);
  }

  @override
  void initState()  {
    _model.userSubject.listen((bool isAuthenticated) {
      setState(() {
        _isAuthenticated = isAuthenticated;
      });
    });


     _getBatteryLevel();

    _model.autoAuth();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        title: 'Easy list',
        routes: {
          '/': (context) {
            return !_isAuthenticated
                ? AuthPage()
                : ProductsPage(
                    model: _model,
                  );
          },
          '/manage-product': (context) =>
              !_isAuthenticated ? AuthPage() : ManageProduct(),
        },
        // only called for named routes
        // and the named route is not listed in the routes
        onGenerateRoute: (RouteSettings settings) {
          final List<String> pathElements = settings.name.split('/');

          if (pathElements[0] != '') {
            return null;
          } else if (pathElements[1] == 'product') {
            final String productId = pathElements[2];

            final Product product =
                _model.allProducts.firstWhere((Product product) {
              return product.id == productId;
            });

            return CustomRoute<bool>(
              builder: (context) => !_isAuthenticated
                  ? AuthPage()
                  : ProductPage(
                      product: product,
                    ),
            );
          }

          return null;
        },

        // called when the resolved route = null
        onUnknownRoute: (RouteSettings settings) {
          return MaterialPageRoute(
            builder: (context) => !_isAuthenticated
                ? AuthPage()
                : ProductsPage(
                    model: _model,
                  ),
          );
        },
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Colors.blueAccent,
        ),
      ),
    );
  }
}
