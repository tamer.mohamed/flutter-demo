import 'package:flutter/material.dart';

import 'location_data.dart';

class Product {
  final String id;
  final String title;
  final String desc;
  final double price;
  final String imageUrl;
  final String imagePath;
  final bool isFavourite;
  final String userEmail;
  final String userId;
  final LocationData location;

  Product({
    @required this.id,
    @required this.title,
    @required this.desc,
    @required this.price,
    @required this.imageUrl,
    @required this.imagePath,
    @required this.userEmail,
    @required this.userId,
    this.location,
    this.isFavourite = false,
  });
}
