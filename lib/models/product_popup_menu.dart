import 'package:flutter/material.dart';

class ProductPopupMenu {
  ProductPopupMenu({this.title, this.icon});

  String title;
  IconData icon;
}