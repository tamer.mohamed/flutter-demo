import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'dart:io';
import 'package:mime/mime.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../models/product.dart';
import '../models/location_data.dart';
import '../models/user.dart';
import '../shared/global_config.dart';

const URL = 'https://flutter-api-6957e.firebaseio.com/';

mixin ConnectedProducts on Model {
  User _authenticatedUser;
  String _currentProductId;
  bool _isLoading = false;
  List<Product> _products = [];

  bool get isLoading {
    return _isLoading;
  }
}

mixin ProductsModel on ConnectedProducts {
  // TODO: move tabs state to separate model
  int _currentSelectedTab = 0;

  List<Product> get allProducts {
    return List.from(_products);
  }

  String get currentProductId {
    return _currentProductId;
  }

  Product get selectedProduct {
    if (_currentProductId == null) {
      return null;
    }

    return _products.firstWhere((Product product) {
      return product.id == _currentProductId;
    });
  }

  int get currentSelectedTab {
    return _currentSelectedTab;
  }

  Future<Null> fetchProducts({clearExisting = false}) async {
    _isLoading = true;

    if (clearExisting) {
      _products = [];
    }

    try {
      final http.Response response =
          await http.get('$URL/products.json?auth=${_authenticatedUser.token}');

      Map<String, dynamic> productListData = json.decode(
        response.body,
      );

      final List<Product> fetchedProductsList = [];

      if (productListData == null || productListData.containsKey('error')) {
        _isLoading = false;
        notifyListeners();
        return;
      }

      productListData.forEach((String key, dynamic value) {
        final Product product = Product(
            id: key,
            title: value['title'],
            desc: value['desc'],
            price: value['price'],
            imageUrl: value['imageUrl'],
            imagePath: value['imagePath'],
            userEmail: value['userEmail'],
            userId: value['userId'],
            location: (value as Map<String, dynamic>).containsKey('location')
                ? LocationData(
                    lng: value['location']['lng'],
                    lat: value['location']['lat'],
                    address: value['location']['address'],
                  )
                : null,
            isFavourite: value['favourites'] != null
                ? (value['favourites'] as Map<String, dynamic>)
                    .containsKey(_authenticatedUser.id)
                : false);

        fetchedProductsList.add(product);
      });

      _products = fetchedProductsList;
      _isLoading = false;

      notifyListeners();

      _currentProductId = null;
    } catch (e) {
      print(e);
    }
  }

  Future<Map<String, dynamic>> _uploadImage(File image,
      {String imagePath}) async {
    final mimeType = lookupMimeType(image.path).split('/');
    final imageUploadRequest = http.MultipartRequest(
      'POST',
      Uri.parse(
        'https://us-central1-flutter-api-6957e.cloudfunctions.net/storeImage',
      ),
    );

    final file = await http.MultipartFile.fromPath(
      'image',
      image.path,
      contentType: MediaType(mimeType[0], mimeType[1]),
    );

    imageUploadRequest.files.add(file);

    if (imagePath != null) {
      imageUploadRequest.fields['imagePath'] = Uri.encodeComponent(imagePath);
    }

    imageUploadRequest.headers['authorization'] =
        'Bearer ${_authenticatedUser.token}';

    try {
      final streamedresponse = await imageUploadRequest.send();
      final response = await http.Response.fromStream(streamedresponse);

      if (response.statusCode != 200 && response.statusCode != 201) {
        print('Something went wrong with upload the image');
        json.decode(response.body);
        return null;
      }

      final responseData = json.decode(response.body);

      return responseData;
    } catch (error) {
      print(error);

      return null;
    }
  }

  Future<bool> addProduct({
    String title,
    String desc,
    File image,
    double price,
    LocationData location,
  }) async {
    _isLoading = true;
    notifyListeners();

    final uploadData = await _uploadImage(image);

    if (uploadData == null) {
      return false;
    }

    try {
      final Map<String, dynamic> productData = {
        'image':
            'https://www.iamexpat.nl/sites/default/files/styles/promo_300x180/public/f79d7f99d72fdc52b093b9a69a4d71ec1459503355.png?itok=GbMV8NNl',
        'title': title,
        'desc': desc,
        'price': price,
        'userEmail': _authenticatedUser.email,
        'userId': _authenticatedUser.id,
        'imagePath': uploadData['imagePath'],
        'imageUrl': uploadData['imageUrl'],
        'location': {
          'lng': location.lng,
          'lat': location.lat,
          'address': location.address
        }
      };

      final http.Response response = await http.post(
        '$URL/products.json?auth=${_authenticatedUser.token}',
        body: json.encode(productData),
      );

      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;
        notifyListeners();
        return false;
      }

      final Map<String, dynamic> responseData = json.decode(response.body);

      Product product = Product(
        id: responseData['name'],
        title: title,
        desc: desc,
        price: price,
        imageUrl: uploadData['imageUrl'],
        imagePath: uploadData['imagePath'],
        userEmail: _authenticatedUser.email,
        userId: _authenticatedUser.id,
        location: location,
      );

      _products.add(product);
      _isLoading = false;
      _currentProductId = null;

      notifyListeners();

      return true;
    } catch (e) {
      _isLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> updateProduct(
      {String title,
      String desc,
      File image,
      double price,
      bool isFavourite = false,
      LocationData location}) async {
    String imageUrl = selectedProduct.imageUrl;
    String imagePath = selectedProduct.imagePath;

    if (image != null) {
      final imageData = await _uploadImage(image, imagePath: imagePath);

      imageUrl = imageData['imageUrl'];
      imagePath = imageData['imagePath'];
    }

    try {
      _isLoading = true;
      notifyListeners();

      Map<String, dynamic> updatedProduct = {
        'title': title,
        'desc': desc,
        'imageUrl': imageUrl,
        'imagePath': imagePath,
        'price': price,
        'userEmail': _authenticatedUser.email,
        'userId': _authenticatedUser.id,
        'location': {
          'lng': location.lng,
          'lat': location.lat,
          'address': location.address
        }
      };

      final http.Response response = await http.put(
          '$URL/products/${selectedProduct.id}.json?auth=${_authenticatedUser.token}',
          body: json.encode(updatedProduct));

      if (response.statusCode != 200 && response.statusCode != 201) {
        _isLoading = false;
        notifyListeners();
        return false;
      }

      Product product = Product(
        id: selectedProduct.id,
        title: title,
        desc: desc,
        imageUrl: imageUrl,
        imagePath: imagePath,
        price: price,
        userEmail: _authenticatedUser.email,
        userId: _authenticatedUser.id,
        location: location,
      );

      _isLoading = false;
      _products[selectedProductIndex] = product;

      notifyListeners();

      return true;
    } catch (e) {
      _isLoading = false;
      notifyListeners();
      return false;
    }
  }

  Future<bool> deleteProduct() async {
    try {
      _isLoading = true;
      final deletedProductId = selectedProduct.id;
      _products.removeAt(selectedProductIndex);
      _currentProductId = null;
      notifyListeners();

      await http.delete(
          '$URL/products/$deletedProductId.json?auth=${_authenticatedUser.token}');

      _isLoading = false;
      notifyListeners();

      return true;
    } catch (e) {
      _isLoading = false;
      notifyListeners();
      return false;
    }
  }

  void setSelectedTab(index) {
    _currentSelectedTab = index;

    notifyListeners();
  }

  void setSelectedProduct(String productId) {
    _currentProductId = productId;

    if (productId != null) {
      notifyListeners();
    }
  }

  List<Product> getFavourites() {
    return List.from(
      _products.where((product) => product.isFavourite).toList(),
    );
  }

  int get selectedProductIndex {
    return _products
        .indexWhere((Product product) => product.id == _currentProductId);
  }

  void toggleProductFavourite() async {
    final bool newFavState = !selectedProduct.isFavourite;

    Product updatedProduct = Product(
      id: selectedProduct.id,
      title: selectedProduct.title,
      desc: selectedProduct.desc,
      location: selectedProduct.location,
      price: selectedProduct.price,
      imageUrl: selectedProduct.imageUrl,
      imagePath: selectedProduct.imagePath,
      isFavourite: newFavState,
      userId: _authenticatedUser.id,
      userEmail: _authenticatedUser.email,
    );

    _products[selectedProductIndex] = updatedProduct;

    http.Response response;

    if (newFavState) {
      response = await http.put(
        '$URL/products/${selectedProduct.id}/favourites/${_authenticatedUser.id}.json?auth=${_authenticatedUser.token}',
        body: json.encode(true),
      );
    } else {
      response = await http.delete(
          '$URL/products/${selectedProduct.id}/favourites/${_authenticatedUser.id}.json?auth=${_authenticatedUser.token}');
    }

    if (response.statusCode != 200 && response.statusCode != 201) {
      Product updatedProduct = Product(
        id: selectedProduct.id,
        title: selectedProduct.title,
        desc: selectedProduct.desc,
        price: selectedProduct.price,
        imageUrl: selectedProduct.imageUrl,
        imagePath: selectedProduct.imagePath,
        isFavourite: !newFavState,
        userId: _authenticatedUser.id,
        userEmail: _authenticatedUser.email,
      );

      _products[selectedProductIndex] = updatedProduct;
    }

    // we have to notify listeners to get live updates
    notifyListeners();
  }
}

mixin UserModel on ConnectedProducts {
  Timer _authTimer;
  PublishSubject<bool> _userSubject = PublishSubject();

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  User get authenticatedUser {
    return _authenticatedUser;
  }

  Future<Map<String, dynamic>> login(String email, String password) async {
    try {
      _isLoading = true;
      notifyListeners();

      final Map<String, dynamic> authData = {
        'email': email,
        'password': password,
        'returnSecureToken': true
      };

      final http.Response response = await http.post(
        'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=$FIREBASE_API_KEY',
        body: json.encode(authData),
        headers: {'Content-Type': 'application/json'},
      );

      final Map<String, dynamic> responseData = json.decode(response.body);
      bool hasError = true;
      String message = 'Invalid credentials';

      if (responseData.containsKey('idToken')) {
        hasError = false;
        message = "Authenticated succesfully";

        _userSubject.add(true);

        saveUserDataIntoPref(responseData);
      } else if (responseData['error']['message'] == 'INVALID_PASSWORD') {
        message = 'This password is invalid';
      } else if (responseData['error']['message'] == 'EMAIL_NOT_FOUND') {
        message = 'This email was not found';
      }

      _isLoading = false;
      notifyListeners();

      return {'success': !hasError, 'message': message};
    } catch (e) {
      _isLoading = false;
      notifyListeners();

      return {'success': false, 'message': 'Something went wrong'};
    }
  }

  Future<Map<String, dynamic>> signUp(String email, String password) async {
    final Map<String, dynamic> authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    try {
      _isLoading = true;
      notifyListeners();

      final http.Response response = await http.post(
          'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=$FIREBASE_API_KEY',
          body: json.encode(authData),
          headers: {'Content-Type': 'application/json'});

      final Map<String, dynamic> responseData = json.decode(response.body);
      bool hasError = true;
      String message = 'Something went wrong';

      if (responseData.containsKey('idToken')) {
        hasError = false;
        message = "Authenticated succesfully";

        _userSubject.add(true);

        saveUserDataIntoPref(responseData);
      } else if (responseData['error']['message'] == 'EMAIL_EXISTS') {
        message = 'This email already exists';
      }

      _isLoading = false;
      notifyListeners();

      return {"success": !hasError, 'message': message};
    } catch (e) {
      _isLoading = false;
      notifyListeners();

      return {"success": false};
    }
  }

  void logout() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    preferences.remove('token');
    preferences.remove('userEmail');
    preferences.remove('userId');

    _authenticatedUser = null;
    _currentProductId = null;
    _userSubject.add(false);
  }

  void autoAuth() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    final String token = preferences.getString('token');
    final String userEmail = preferences.getString('userEmail');
    final String userId = preferences.getString('userId');
    final String expireyTime = preferences.getString('expireyTime');

    if (token != null) {
      final DateTime now = DateTime.now();
      final parsedExpiryTime = DateTime.parse(expireyTime);

      if (parsedExpiryTime.isBefore(now)) {
        _authenticatedUser = null;
      } else {
        final tokenLifeSpan = parsedExpiryTime.difference(now).inSeconds;

        // setup the timer for the remaining seconds
        setAuthTimer(tokenLifeSpan);
        _authenticatedUser = User(
          id: userId,
          email: userEmail,
          token: token,
        );

        _userSubject.add(true);
      }

      notifyListeners();
    }
  }

  void setAuthTimer(int time) {
    _authTimer = Timer(Duration(seconds: time), logout);
  }

  String getExpiryTime(int expiresIn) {
    final DateTime now = DateTime.now();
    final DateTime expiryTime = now.add(Duration(seconds: expiresIn));

    return expiryTime.toIso8601String();
  }

  void saveUserDataIntoPref(Map<String, dynamic> userData) async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    final int expiresIn = int.parse(userData['expiresIn']);

    preferences.setString('token', userData['idToken']);
    preferences.setString('userEmail', userData['email']);
    preferences.setString('userId', userData['localId']);
    preferences.setString('expireyTime', getExpiryTime(expiresIn));

    // for auto logout when the token is expired;
    setAuthTimer(expiresIn);

    _authenticatedUser = User(
      id: userData['localId'],
      email: userData['email'],
      token: userData['idToken'],
    );
  }
}
