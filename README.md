# Flutter demo

Exploring Flutter & Dart capabilities

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.


## Running the app on simulator (iOS and Android)

- Install [flutter](https://flutter.dev/docs/get-started/install) 
- open the project in intelliJ or Android studio editor 
- rename file `lib/global_config.example.dart` to `lib/global_config.dart` and replace the API key placeholders
- select your target simulator device
- run the app from your editor!


